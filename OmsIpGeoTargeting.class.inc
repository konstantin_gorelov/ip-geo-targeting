<?php
namespace Ipgeo;

/**
 * Перегружает методы, возвращающие запросы к БД.<br> 
 * Получает дополнительные данные из OMS: oms_city_id и cms_publication_level
 * @author gorelov.k
 */
class OmsIpGeoDbQuery extends IpGeoDbQuery{
	/**
	 * SQL-запрос на положение по коду<br> 
	 * Получает дополнительные данные из OMS: oms_city_id и cms_publication_level
	 * @param string $code
	 * @param int $set
	 * @return string Запрос на положение по коду
	 */
	public function selectByCode($code,$set = 0){
		$sql = '
SELECT  
	city_id AS oms_city_id, 
	city_external_id AS cms_publication_level,  

	geoip_country_id   ,  
	geoip_area_id	  ,  
	geoip_region_id	,  
	geoip_city_id	  ,  
	geoip_location_id  ,  
	geoip_location_type, 
	geoip_location_code,  
	geoip_location_meta,  
	geoip_longitude	,  
	geoip_lattitude	,  
	geoip_city_name	,  
	geoip_region_name  ,  
	geoip_area_name	,  
	geoip_country_name ,  
	geoip_country_code   
FROM  
	geoip_denormalized_targets 
	LEFT JOIN 
	oms_cities 
	USING (geoip_location_id) 
WHERE	 
	geoip_location_code = \'%s\' 
ORDER BY 
	geoip_location_type DESC 
LIMIT 1 
';
		return sprintf($sql, pg_escape_string($code));
	}
	
	/**
	 * SQL-запрос на положение по идентификатору<br> 
	 * Получает дополнительные данные из OMS: oms_city_id и cms_publication_level
	 * @param string $id
	 * @param int $set
	 * @return string Запрос на положение по иентификатору
	 */
	public function selectByIdentity($id,$set = 0){
		$sql = '
SELECT  
	city_id AS oms_city_id, 
	city_external_id AS cms_publication_level,  

	geoip_country_id   ,  
	geoip_area_id	  ,  
	geoip_region_id	,  
	geoip_city_id	  ,  
	geoip_location_id  ,  
	geoip_location_type, 
	geoip_location_code,  
	geoip_location_meta,  
	geoip_longitude	,  
	geoip_lattitude	,  
	geoip_city_name	,  
	geoip_region_name  ,  
	geoip_area_name	,  
	geoip_country_name ,  
	geoip_country_code   
FROM  
	geoip_denormalized_targets 
	LEFT JOIN 
	oms_cities 
	USING (geoip_location_id) 
WHERE	 
	geoip_location_id = %d
ORDER BY 
	geoip_location_type DESC 
LIMIT 1 
';
		return sprintf($sql, $id);
	}

	/**
	 * SQL-запрос на положение по OMS CITY ID<br> 
	 * @param string $oms_city_id
	 * @param int $set
	 * @return string Запрос на положение по иентификатору
	 */
	public function selectByOmsCityId($oms_city_id,$set = 0){
		$sql = '
SELECT  
	city_id AS oms_city_id, 
	city_external_id AS cms_publication_level,  

	geoip_country_id   ,  
	geoip_area_id	  ,  
	geoip_region_id	,  
	geoip_city_id	  ,  
	geoip_location_id  ,  
	geoip_location_type, 
	geoip_location_code,  
	geoip_location_meta,  
	geoip_longitude	,  
	geoip_lattitude	,  
	geoip_city_name	,  
	geoip_region_name  ,  
	geoip_area_name	,  
	geoip_country_name ,  
	geoip_country_code   
FROM  
	geoip_denormalized_targets 
	LEFT JOIN 
	oms_cities 
	USING (geoip_location_id) 
WHERE	 
	oms_cities.city_id = %d
ORDER BY 
	geoip_location_type DESC 
LIMIT 1 
';
		return sprintf($sql, $oms_city_id);
	}

	/**
	 * SQL-запрос на положение по IP<br> 
	 * Получает дополнительные данные из OMS: oms_city_id и cms_publication_level
	 * @param string $ip
	 * @param int $set
	 * @return string Запрос на положение по IP
	 */
	public function selectByIp($ip,$set = 0){
		//	-- фокусы с коализами нужны, чтобы прошло сравнеие для значений с городом NULL 
		//	-- (это когда для диапозона страна есть, а город не известен) 
		//	-- жесткое сравнение допустимо, так как в нормализованной структуре диапозонов  
		//	-- обязана существовать запись, у которой либо задан город и страна,  
		//	-- либо задана страна, а город - null  
		$sql = '
SELECT 
	city_id AS oms_city_id, 
	city_external_id AS cms_publication_level,  

	geoip_country_id   , 
	geoip_area_id	  , 
	geoip_region_id	, 
	geoip_city_id	  , 
	geoip_location_id  , 
	geoip_location_type, 
	geoip_location_code, 
	geoip_location_meta, 
	geoip_longitude	, 
	geoip_lattitude	, 
	geoip_city_name	, 
	geoip_region_name  , 
	geoip_area_name	, 
	geoip_country_name , 
	geoip_country_code  
FROM 
	geoip_denormalized_targets 
	LEFT JOIN 
	oms_cities ci 
	USING(geoip_location_id) 
WHERE	 
	-- фокусы с коализами нужны, чтобы прошло сравнеие для значений с городом NULL  
	-- (это когда для диапозона страна есть, а город не известен)  
	-- жесткое сравнение допустимо, так как в нормализованной структуре диапозонов   
	-- обязана существовать запись, у которой либо задан город и страна,   
	-- либо задана страна, а город - null   
	(COALESCE(geoip_city_id,0),geoip_country_id) = (	
		SELECT	
		COALESCE(geoip_city_id,0),	
		geoip_country_id	
		FROM  
		geoip_ip_blocks	 
		WHERE	 
		geoip_block_start <= %1$u AND %1$u <= geoip_block_end	
	)	
';
		return sprintf($sql, ip2long($ip));
	}
	
}

class OmsIpGeoTargeting extends IpGeoTargeting {
	
	/**
	 * Инициализирует параметры, нужные для работы.
	 * @param String $dbConnectString Строка подключения к БД вроде 'host=localhost port=5432 dbname=photoholding_orders_management user=photoholding_orders_management_master password=***'
	 * @param String $http_host Домен для куки. По умолчанию '' - любой.
	 * @param String $ttl 2592000 Время жизни куки в сек. 
	 */
	public static function configure($dbConnectString, $http_host, $ttl = 2592000) {
		self::$_location = new IpGeoLocation();
		self::$_cookie	= new IpGeoCookies('_geoiplocation', $ttl, $http_host);
		self::$_dbConnectString = $dbConnectString;
		self::$_query = new OmsIpGeoDbQuery;
		return true;
	}
	/** 
	 * Определяет и загружает параметры положения по oms city id
	 * @param oms_city_id ID города OMS, для которого надо определить положение
	 * @return boolean true, если запрос вернул данные; иначе false
	 * @deprecated
	 */
	public static function getLocationByOmsCityId($oms_city_id) {
//		self::$_location->geoip_detection_method = 'ip';
//		self::$_location->geoip_location_ip = $ip_address;
//		self::headerReport('X-GEO-LOCATION-IP',self::$_location->geoip_location_ip);
		
		$q = self::$_query->selectByOmsCityId($oms_city_id);
//		$q = sprintf($sql,$oms_city_id);
//		echo '<pre>',$q;
		return self::queryGeoData($q);
	}
}
?>