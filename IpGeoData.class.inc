<?php 

namespace Ipgeo;

class IpGeoData {
	
	private $cityName;
	private $metaData;
	private $locationCode;
	
	public function __construct($cn, $md, $lc) {
		$this->cityName = $cn;
		$this->metaData = $md;
		$this->locationCode = $lc;
	}
	
	
	public function getCityName() {
		return $this->cityName;
	}
	
	public function getMetaData() {
		return json_decode($this->metaData);
	}
	
	public function getLocationCode() {
		return $this->locationCode;
	}
	
}
