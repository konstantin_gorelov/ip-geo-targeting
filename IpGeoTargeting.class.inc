<?php 
namespace Ipgeo;


/**
 * Вспомогательный класс для удобной работы с точками входа: <br>
 * позволяет сопоставить с кодом точки входа из URI имя файла и тип точки - виртуальная или реальная.
 * @author gorelov.k
 */
class SiteEntryPoint{
	const EP_REAL = 'real';
	const EP_VIRTUAL = 'virtual';
	/** Точка входа типа 'ru' */
	public $code;
	/** Query string */
	public $query_string;
	/** Путь из адресной строки */
	public $path;
	/** Тип точки входа: реальная (есть файл, ts) или виртуальная (нет файла, moscow) */
	public $type;
	/** Файл, соответствующий точке входа на файловой системе */
	public $file;
	
	public function __construct($uri){
		$this->parseUri($uri);
	}
	/** Для удобства устанавливает сразу оба свойства объекта */
	public function set($code,$file = null){
		$this->code = $code;
		$this->file = $file;
		$this->type = $this->file ? self::EP_REAL : self::EP_VIRTUAL;
	}
	public function parseUri($request_uri = null) {
		if($request_uri == null) $request_uri = $_SERVER['REQUEST_URI'];
		//Разбиваем URI на части
		$this->query_string	= strstr($request_uri,'?');
		//Может быть запрос без ?
		$this->path			= $this->query_string ? strstr($request_uri,'?',true) : $request_uri;
		//Эта точка входа указана в URI - тип /spb/... или /spb?... или /spb, но не /name.ext
		$this->code	= explode('/',$this->path)[1];
		//Здесь стираем все, что похоже на имя файла (то есть, не точка входа)
		$this->code = preg_replace('#[a-z]+\.[a-z]+#','',$this->code);
		return $this->code;
	}
}

/**
 * Данные местоположения и операции для работы с ними
 * 
 * @author gorelov.k
 */
class IpGeoLocation {
	/** String Рассказывает, каким образом было определено положение */
	public $geoip_detection_method;
	public $geoip_location_ip;
	public $geoip_country_id;
	public $geoip_area_id;
	public $geoip_region_id;
	public $geoip_city_id;
	public $geoip_location_id;
	public $geoip_location_type; 
	public $geoip_location_code; 
	public $geoip_location_meta; 
	public $geoip_longitude;
	public $geoip_lattitude;
	public $geoip_city_name;
	public $geoip_region_name; 
	public $geoip_area_name;
	public $geoip_country_name;
	public $geoip_country_code;

	/**
	 * Возвращает URI, соответствующий текущему положению (берет то, что лежит в $this->geoip_* и как-то делает из этого URI)<br>
	 * Разумно использовать для того, чтобы генерить URI точек входа.<br>
	 * По умолчанию возвращает URI вроде /moscow
	 * @return string URI, соответствующий текущему положению на основе данных из структуры положения $this->geoip_*<br>
	 * null в случае ошибки
	 */
	public function getLocationURI() {
		if($this->geoip_location_code) return '/'.$this->geoip_location_code;
		else return null;
	}
	
	/**
	 * Сбрасывает все свойства объекта IpGeoLocation
	 */
	public function reset() {
		foreach ($this as $k=>$v) {
			$this->{$k} = null;
		}
	}
	
	/** 
	 * Возвращает строку с идентификацией положения, что надо записать в куку.<p> 
	 * Может быть перегружен, если надо как-то иначе идентифицировать положение.
	 * По умолчанию возвращает текущий geoip_location_id</p>
	 * @return Данные, идентифицирующие текущее положение. <br>
	 * null в случае ошбки (если $this->geoip_location_id не задан)
	 */
	public function encodeLocationIdentity() {
		if($this->geoip_location_id) return $this->geoip_location_id;
		else return null;
	}
	
	/** 
	 * Расшифровывает то, что было записано в куку, и заносит полученные данные в объект IpGeoLocation
	 * По умолчанию ожидает в куке (int) geoip_location_id и сохраняет его в текущей структуре. 
	 * В случае успеха возвращает true. 
	 * @param str String Строка, где хранится идентификация положения
	 * @return boolean Возвращает истину, если найденный geoip_location_id больше нуля
	 */
	public function decodeLocationIdentity($str) {
		return ($this->geoip_location_id = (int)$str) > 0;
	}

}

/**
 * Операции и данные для работы с куками
 * 
 * @author gorelov.k
 */
class IpGeoCookies {
	private $_cookie_name;
	/** Время жизни куки в секундах */
	private $_cookie_ttl;
	private $_cookie_path;
	private $_cookie_domain;
	private $_domain_lookup;

	/** 
	 * Задает параметры для гео-куки
	 * 
	 * @param $name String Имя, которое использовать для куков. По умолчанию _geoiplocation
	 * @param $ttl int Время жизни гео куки в секундах, по умолчанию месяц
	 * @param $http_host String Домен для куки. По умолчанию '' - любой.
	 * @param $path String Путь для куки. По умолчанию /
	 * @param $preg String Регэксп для выявления значимой части домена. По умолчанию второй уровень - #[^.]+\.[^.]+$#
	 */
	public function __construct($name = '_geoiplocation', $ttl = 2592000, $http_host = '', $path = '/', $preg = '#[^.]+\.[^.]+$#') {
		$this->_domain_lookup	= $preg;
		$this->_cookie_name		= $name;
		$this->_cookie_ttl		= $ttl;
		$this->_cookie_path		= $path;
		if($http_host) $this->_cookie_domain = $this->setupCookieDomain($http_host, $preg);
	}
	
	/**
	 * Настраивает домен для куки. Находит в $http_host значение по регулярке из $preg и добавляет ведущую точку.<br>
	 * Таким образом можно, например, для всех доменов с одним и тем же вторым уровнем ставить общедоступные куки.
	 * @param String $http_host Хост (обычно $_SERVER['HTTP_HOST']), в котором искать правильный домен.
	 * @param String $preg Регулярное выражение для поиска домена. Например, выделяет домен второго уровня.
	 * @return String Правильный домен для куки
	 */
	protected function setupCookieDomain($http_host, $preg = '#[^.]+\.[^.]+$#') {
		preg_match($preg, $http_host, $matches);
		if ($matches[0]) return '.'.$matches[0];
		else return '';
	}
	
	/** 
	 * Читает данные из куки положения, никак эти данные не интерпретирует. 
	 * @return String То, что нашлось в куке.
	 */
	public function getGeoCookie() {
		return $_COOKIE[$this->_cookie_name];
	}
	
	/** Устанавливает куку положения
	* 
	* @param value String Значение куки, если null или не задано, кука будет удалена
	* @return boolean flase в случе ошибки ($value === null или setcookie() не сработал)
	*/
	public function setGeoCookie($value,$name = null) {
//		if($value === null) return false;
		if($name == null) $name = $this->_cookie_name;
		return setcookie($name, $value, time()+$this->_cookie_ttl, $this->_cookie_path, $this->_cookie_domain);
	}
}

/** 
 * Обеспечивает создание различных по структуре запросов к Гео-БД.<br>
 * Упрощенный вариант - запросы хардкодные, зато кристально понятно.
 * 
 * @author gorelov
 */
class IpGeoDbQuery {

	/**
	 * SQL-запрос на положение по коду
	 * @param string $code
	 * @param int $set
	 * @return string Запрос на положение по коду
	 */
	public function selectByCode($code,$set = 0){
		$sql = '
SELECT 
    geoip_country_id   , 
    geoip_area_id      , 
    geoip_region_id    , 
    geoip_city_id      , 
    geoip_location_id  , 
    geoip_location_type, 
    geoip_location_code, 
    geoip_location_meta, 
    geoip_longitude    , 
    geoip_lattitude    , 
    geoip_city_name    , 
    geoip_region_name  , 
    geoip_area_name    , 
    geoip_country_name , 
    geoip_country_code  
FROM 
    geoip_denormalized_targets 
WHERE    
	geoip_location_code = \'%s\'
ORDER BY
	geoip_location_type DESC
LIMIT 1
';
		return sprintf($sql, pg_escape_string($code));
	}
	
	/**
	 * SQL-запрос на положение по идентификатору
	 * @param string $id
	 * @param int $set
	 * @return string Запрос на положение по коду
	 */
	public function selectByIdentity($id,$set = 0){
		$sql = '
SELECT 
    geoip_country_id   , 
    geoip_area_id      , 
    geoip_region_id    , 
    geoip_city_id      , 
    geoip_location_id  , 
    geoip_location_type, 
    geoip_location_code, 
    geoip_location_meta, 
    geoip_longitude    , 
    geoip_lattitude    , 
    geoip_city_name    , 
    geoip_region_name  , 
    geoip_area_name    , 
    geoip_country_name , 
    geoip_country_code  
FROM 
    geoip_denormalized_targets 
WHERE    
	geoip_location_id = %d
LIMIT 1
';
		return sprintf($sql, $id);
	}
	
	/**
	 * SQL-запрос на положение по IP
	 * @param string $ip
	 * @param int $set
	 * @return string Запрос на положение по коду
	 */
	public function selectByIp($ip,$set = 0){
		//	-- фокусы с коализами нужны, чтобы прошло сравнеие для значений с городом NULL 
		//	-- (это когда для диапозона страна есть, а город не известен) 
		//	-- жесткое сравнение допустимо, так как в нормализованной структуре диапозонов  
		//	-- обязана существовать запись, у которой либо задан город и страна,  
		//	-- либо задана страна, а город - null  
		$sql = '
SELECT 
    geoip_country_id   , 
    geoip_area_id      , 
    geoip_region_id    , 
    geoip_city_id      , 
    geoip_location_id  , 
    geoip_location_type, 
    geoip_location_code, 
    geoip_location_meta, 
    geoip_longitude    , 
    geoip_lattitude    , 
    geoip_city_name    , 
    geoip_region_name  , 
    geoip_area_name    , 
    geoip_country_name , 
    geoip_country_code  
FROM 
    geoip_denormalized_targets 
WHERE    
	(COALESCE(geoip_city_id,0),geoip_country_id) = (	
		SELECT	
		COALESCE(geoip_city_id,0),	
		geoip_country_id	
		FROM 
		geoip_ip_blocks	
		WHERE	
		geoip_block_start <= %1$u AND %1$u <= geoip_block_end	
	)
';
		return sprintf($sql, ip2long($ip));
	}
}

/**
 * Определяет местоположение юзера по параметрам юзера:
 * - по IP, если никаких других наметок нет
 * - по точке входа, если указана
 * - по данным в гео-куке
 * 
 * @author gorelov.k
 *
 */
class IpGeoTargeting {
	/** Строка коннекта к базе данных */
	protected static $_dbConnectString;
	/** Коннект к базе данных */
	protected static $_dbConnection;
	/** Данные о местоположении - объект */
	protected static $_location;
	/** Методы и параметры, нужные для работы с куками */
	protected static $_cookie;
	/** Запросы к базе данных */
	protected static $_query;
	/** Положение по умолчанию - если из базы ничего не было загружено */
	public static $default_location = 'ru';
	
	/** 
	 * Инициализирует параметры, нужные для работы
	 * @param dbConnectString
	 */
	public static function configure($dbConnectString, $http_host, $ttl = 2592000) {
		self::$_location = new IpGeoLocation();
		self::$_cookie	= new IpGeoCookies('_geoiplocation', $ttl, $http_host);
		self::$_dbConnectString = $dbConnectString;
		self::$_query = new IpGeoDbQuery;
		return true;
	}
	
	/** 
	 * Определяет местоположение по точке входа, IP, cookie.
	 * 
	 * Заполняет структуру IpGeoLocation. Устанавливает куку в новое значение.<br>
	 * Если положение найти не удалось (не найден IP или что-то не настроено в базе), структура остается пустой.<br>
	 * 
	 * @param String $location_code Код местоположения (у нас - точка входа)
	 * @param String $ip_address IP-адрес в виде 123.45.67.890
	 * @return boolean Возвращает false, если указанный location_code (точка входа) не найден; true в случае успеха. <b>(!)Если запрос к базе обломался, то все равно возвращает true</b>  
	 */
	public function detectLocation($location_code, $ip_address) {
		if($location_code == ''){
			// В URI НЕ ЗАДАНА точка, то непонятно, где мы. Надо определять.
			if(IpGeoTargeting::getLocationCookie()){
				// Кука с идентификацией положения получена успешно.
				// Достаем параметры из базы
				// TODO: анализировать ошибки - соединие с базлом
				IpGeoTargeting::getLocationByIdentity();
			}else{
				// By IP: Нет ни куки, ни точки входа - надо найти положение по IP (других вариантов нет)
				IpGeoTargeting::getLocationByIP($_SERVER['REMOTE_ADDR']);
			}
		}else{
			// В URI ЗАДАНА точка входа , которая похожа на город: юзер явно задал положение в URL (букмарки, реклама)
			// Уточнаяем положение для этой точки входа.
			if(IpGeoTargeting::getLocationByCode($location_code)){
				// Удалось найти параметры положения по точке входа: 
				// знаем плид и сити_ид для нетпринта, они живут в IpGeoTargeting::location
			}else{
				// Не нашли положением с таким кодом - 404
				// Юзер впечатал или откуда-то взял точку входа, не соответствующую ничему
				return false;
			}
		}
		self::headerReport('LOCATION-DETECTED','['.self::$_location->getLocationURI().']');
		// К этому моменту определили параметры положения и точку входа,
		// устанавливаем куку в т.ч. чтобы заменить неправильную
		IpGeoTargeting::setLocationCookie();
		// FIXME: это непонятная доработка, откуда взялось? Зачем надо?
		IpGeoTargeting::setLocationNameCookie();
		return true;
	}
	
	/** 
	 * Возвращает объект с характеристиками найденного местоположения.<br>
	 * Вообще возвращает свойство protected static $_location, так как оно static,
	 * можно всегда обращаться через базовый класс IpGeo\IpGeoTargeting, 
	 * но вернется правильный объект потомка класса IpGeoLocation.
	 * Идея в том, чтобы те, кто пользует API,
	 * минимально запаривались знаниями особенностей отдельных проектов.
	 * @example IpGeo\IpGeoTargeting::location()->oms_city_id;
	 * @return IpGeoLocation
	 */
	public static function location() {
		return IpGeoTargeting::$_location;
	}
	
	/** 
	 * Читает из куки данные положения. Использует location->decodeLocationIdentity() чтобы расшифровать куку.
	 * @return boolean Возвращает истину, если в данных из куки удалось найти идентификатор положения.
	 */
	public static function getLocationCookie() {
		return self::$_location->decodeLocationIdentity(self::$_cookie->getGeoCookie());
	}
	
	/** 
	 * Устанавливает куку положения
	 */
	public static function setLocationCookie() {
//		if($f) var_dump(self::$_cookie,self::$_location->encodeLocationIdentity());
		return self::$_cookie->setGeoCookie(self::$_location->encodeLocationIdentity());
	}
	
	/** 
	 * Устанавливает куку положения - справочно (строковый вид)
	 * K: Зачем нужен? Кто добавил? Почему нет в модели? 
	 */
	public static function setLocationNameCookie() {
		return self::$_cookie->setGeoCookie(self::$_location->geoip_region_name,'_geoiplocationname');
	}
	
	/** 
	 * Определяет и загружает параметры положения по IP
	 * @param ip_address IP адрес, для которого надо определить положение
	 * @return boolean true если все в порядке, false в случае ошибки запроса к базе
	 */
	public static function getLocationByIP($ip_address) {
		self::$_location->geoip_detection_method = 'ip';
		self::$_location->geoip_location_ip = $ip_address;
		self::headerReport('LOCATION-IP',self::$_location->geoip_location_ip);
		
		$sql = self::$_query->selectByIp($ip_address);
		$q = sprintf($sql,ip2long($ip_address),ip2long($ip_address));
		return self::queryGeoData($q);
	}

	/** 
	 * Поиск по точке входа - выдаем самое мелкое найденное положение из существующих, если есть много положений с одной точкой входа
	 * @param location Объект класса IpGeoLocation, определяющий местоположение
	 */
	public static function getLocationByCode($location_code) {
		self::$_location->geoip_detection_method = 'code';
		$sql = self::$_query->selectByCode($location_code);
		self::$_location->geoip_location_code = $location_code;
		self::headerReport('LOCATION-CODE',self::$_location->geoip_location_code);
		
		$q = sprintf($sql,pg_escape_string(self::$_location->geoip_location_code)); 
		return self::queryGeoData($q);
	}
	
	/** 
	 * Загружает параметры положения по "идетификации" - набору парамтров, используемых для куков. 
	 * 
	 * @return boolean Возвращает true в случае успеха, false в случае ошибки.
	 */
	public static function getLocationByIdentity() {
		self::headerReport('LOCATION-ID',self::$_location->geoip_location_id);
		self::$_location->geoip_detection_method = 'identity';
		
		$q = self::$_query->selectByIdentity(self::$_location->encodeLocationIdentity());
		return self::queryGeoData($q);
	}

	/**
	 * Записывает заголовки для редиректа на URL и завершает исполнение скрипта.<br>
	 * Устанавливает HTTP-заголовки X-GEO-LOCATION-ID и X-GEO-LOCATION-IP.
	 * @see IpGeoLocation->getLocationURI()
	 * @param url URL, на который надо перейти
	 * @return boolean Возвращает false в случе ошибки (не задан $url) 
	 */
	public static function redirectAndExit($url) {
		if(!$url) return false;
		ob_clean();
		self::headerReport('LOCATION-ID',self::$_location->geoip_location_id);
		self::headerReport('LOCATION-IP',self::$_location->geoip_location_ip);
		self::headerReport('LOCATION-CODE',self::$_location->geoip_location_code);
		self::headerReport('LOCATION-REDIRECT',$url);
		header('HTTP/1.0 301 Moved Permanently');
		header(sprintf('Location:%s',$url),true,301);
		die();
	}
	
	/** 
	 * Записывает 404 заголовок, текст из body и завершает исполнение.
	 * @param body То, что надо отдать в ответе на запрос с кодом 404
	 */
	public function reportHttpNotFoundAndExit($body = null) {
		if($body != null) ob_clean();
		header("HTTP/1.0 404 Not Found", true, 404);
		echo $body;
		die();
	}

	/** Служебные методы **/
	
	/**
	 * Загружает все поля, полученные запросом $q, в свойства объекта self::$_location.<br>
	 * Если не удалось загрузить данные, все поля структуры location будут обнулены.<br>
	 * Открывает соединение непосредственно перед запросом и сразу закрывает предполагая,<br>
	 * что для каждого конкретного случая определения гео-положения будет использован один запрос.
	 * @param String $q
	 * @return boolean true, если запрос вернул данные; иначе false
	 */
	protected function queryGeoData($q) {
		if(IpGeoTargeting::dbConnect() == false) return false;
		$r = pg_query(self::$_dbConnection, $q) or trigger_error('	Error @ queryGeoData() '.pg_last_error(self::$_dbConnection));
		IpGeoTargeting::dbClose();
		if($w = pg_fetch_assoc($r)){
			foreach ($w as $k=>$v){
				self::$_location->{$k} = $v;
			}
			self::headerReport('QUERY-GEO-DATA','DATA RECEIVED');
			return true;
		}else{
			// Запрос выполнился, но не вернул никаких данных
			self::$_location->reset();
			self::$_location->geoip_location_code = self::$default_location;
			self::headerReport('QUERY-GEO-DATA','NO GEO-DATA FOUND, default loacation', self::$_location->geoip_location_code);
		}
		return false;
	}
	
	/** Подключаемся к базе всегда - как минимум запрос на проверку должен быть */
	protected static function dbConnect() {
		// "host=sheep port=5432 dbname=mary user=lamb password=foo"
		if(!self::$_dbConnection && self::$_dbConnection = pg_connect(self::$_dbConnectString)) return true;
		return false;
	}
	
	/** Закрываем соединение к базе */
	protected static function dbClose() {
		if(self::$_dbConnection) {
			$result =  pg_close(self::$_dbConnection);
			if($result) self::$_dbConnection = null;
			return $result;
		}
		return true;
	}
	/**
	 * Записывает заголовок $header для отладки. Добавляет фирменный префикс X-GEO-
	 * @param String $header HTTP-Заголовок, например: X-GEO-CONNECTON-ESTABLISHED
	 * @param String ... дополнительные параметры
	 * @example headerReport('SMTHING',$location_code,$city_id);
	 */
	protected static function headerReport($header){
		static $n = 0, $t = 0;
		if(!$t) $t = microtime(true);
		$a = func_get_args();
		header(sprintf('X-GEO-%d-%s: [%.4f] %s',$n++, array_shift($a), microtime(true)-$t, implode(' ',$a)));
		$t = microtime(true);
	}
	
	/** Возвращает имя города - self::$_location->geoip_city_name */
	public static function getCityName() {
		return self::$_location->geoip_city_name;
	}
	/** Возвращает метаданные положения - self::$_location->geoip_location_meta */
	public static function getMetaData() {
		return self::$_location->geoip_location_meta;
	}
	/** Возвращает точку входа - self::$_location->geoip_location_code */
	public static function getEntryPoint() {
		return self::$_location->geoip_location_code;
	}
	/** Показывает параметры, используемые для кук */
	public static function showCookieInfo() {
		var_dump(self::$_cookie);
	}
}


?>